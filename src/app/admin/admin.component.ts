import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSort, Sort } from '@angular/material/sort';
import { ViewChild } from '@angular/core';
import { CompileMetadataResolver } from '@angular/compiler';
import { MatTableDataSource } from '@angular/material/table';

export interface Experimento {
  nomeExperimento: string,
  categoriaExperimento: string,
  ativo: boolean
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  indexData: Experimento[] = [
    { nomeExperimento: "Experimento nº 1", categoriaExperimento: "Física", ativo: true },
    { nomeExperimento: "Experimento nº 2", categoriaExperimento: "Matemática", ativo: true },
    { nomeExperimento: "Experimento nº 3", categoriaExperimento: "Quimíca", ativo: false },
    { nomeExperimento: "Experimento nº 4", categoriaExperimento: "Quimíca", ativo: false },
    { nomeExperimento: "Experimento nº 5", categoriaExperimento: "Matemática", ativo: true },
    { nomeExperimento: "Experimento nº 6", categoriaExperimento: "Matemática", ativo: true },
  ]

  colunas: string[] = ["experimento", "categoria", "ativo"];
  dadosExperimento = this.indexData;
  ordenador: Experimento[];
  dataSource = new MatTableDataSource(this.indexData);

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  ordenar(sort: Sort) {
    const data = this.indexData.slice();
    if(!sort.active || sort.direction === ''){
      this.ordenador = data;
      return;
    }

    this.ordenador = data.sort((a,b)=>{
      const isAsc = sort.direction === 'asc';
      switch(sort.active){
        case 'nomeExperimento': return this.compare(a.nomeExperimento, b.nomeExperimento, isAsc)
        case 'categoriaExperimento': return this.compare(a.categoriaExperimento, b.categoriaExperimento, isAsc)
      }
    });
  }
  
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'search',
      sanitizer.bypassSecurityTrustResourceUrl('./assets/search.svg'));
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }
  
}
